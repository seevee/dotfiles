#!/usr/bin/env bash

function log {
  local default_branch
  local current_branch
  default_branch=$(git default-branch)
  current_branch=$(git rev-parse --abbrev-ref HEAD)
  if [ "$default_branch" == "$current_branch" ]; then
    default_branch="origin/$default_branch"
  fi

  local rst
  local grn
  local red
  rst=$(tput sgr0)
  grn=$(tput setaf 2)
  red=$(tput setaf 1)

  printf "%b" "## ${grn}${current_branch}${rst}...${red}${default_branch}${rst}"
  echo

  git --no-optional-locks lg "$default_branch".. --color
}
export -f log

watch --color --interval 1 --no-title -x bash -c log
