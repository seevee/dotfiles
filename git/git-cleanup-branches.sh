#!/usr/bin/env bash

default_branch=$(git default-branch)
git ss
git co "$default_branch"

# prune remote branches
echo "🥁 Pruning remote branches..."
git fa --prune
echo

# remove merged branches
echo "🥁 Removing merged branches..."
git b --format='%(refname:short)' --merged | \
  grep -v "$default_branch" | \
  xargs -r git branch -d
echo

# prompt to remove branches from other authors
echo "🥁 Removing branches from other authors..."
me=$(git config user.name)
for branch in $(git b | grep -v "$default_branch"); do
  commits=$default_branch..$branch

  if ! git lg "$commits" | grep -q "$me"
  then
    echo "You don't seem to have any commits in the branch  ⑂ $branch"
    echo
    git lg --color "$commits" | sed "s/^/  /g"
    echo
    echo

    read -rp "Delete the branch '$branch'? (y/n)? " choice
    case "$choice" in
      y|Y ) git b -D "$branch";;
      * ) echo "Ok, keeping it.";;
    esac
    echo
  fi
done

git co -
git sp
