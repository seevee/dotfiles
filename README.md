# dotfiles

My personal dotfiles, aiming to provide a system-agnostic development environment.

On Linux, this will also install and configure a DE using i3.

## What's Included

### git config

`git/.gitconfig` is full of many aliases and sane gitconfig defaults

### language installation + configuration

* `node`
* `rust`
* `python`
* `java`

### terminal tools

* `nvim`
* `zsh`/`oh-my-zsh`, `asdf`
* `ack`, `htop`, `fzf`, etc.

### nerd fonts

* Hack
* Source Code Pro

### linux-specific packages

* `i3`, `rofi`, `dunst`, `feh`, `flameshot`
* `i3blocks`
* `i3lock-color`/`betterlockscreen`

### other quirks

* gruvbox theme

## Target Systems

Currently tested against EndeavourOS and Pop! OS

Previously tested against:
* Kubuntu 22.10
* macOS 11 Big Sur (requires [Homebrew](https://brew.sh/))
* Fedora 32

## Installation

This project uses git submodules, so make sure you clone appropriately:

```sh
git clone https://gitlab.com/seevee/dotfiles.git $HOME/.dotfiles --recurse-submodules
```
* I recommended cloning this repo to `$HOME/.dotfiles`, as expected by `oh-my-zsh`

Then do EITHER:
* copy/symlink `i3/`, `dunst/`, `rofi/`, etc. into `~/.config/` as desired
* execute the install script in the project root to install everything:
```sh
./install.sh
```

> **Warning**
> `./install.sh` will overwrite many directories in `~/.config/` with symlinks

> **Note**
> On your first install, you may need to relogin for full functionality

## TODO

* Add wayland/sway support
