#!/usr/bin/env bash

wallpapers="$HOME/Pictures/wallpapers"
if [[ -d "${wallpapers}" ]]; then
  find "$wallpapers" -type f | sort -R | tail -1 | while read -r file; do
    DISPLAY=:0.0 feh --bg-fill "$file"
    hash betterlockscreen 2>/dev/null && betterlockscreen -u "$file"
  done
fi
