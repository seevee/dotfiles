#!/usr/bin/env bash
# make virtual audio sinks

sinks=(
  "game_audio"
  "other_audio"
  "alerts"
  "music"
)

# Pipewire invocation had severe distortion prior to 0.3.77
# seems to be fixed now
pipewire_loopback () {
  pw-loopback \
    --capture-props="media.class=Audio/Sink node.description=$1 node.name=$1" &
}

# pulseaudio equivalent with pactl, uses pipewire under the hood
pulseaudio_load_module () {
  pactl load-module module-remap-sink sink_name="$1"
}

for sink in "${sinks[@]}"; do
  pipewire_loopback "$sink"
  # pulseaudio_load_module "$sink"
  echo "Created virtual sink: $sink"
done
