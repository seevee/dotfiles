#!/usr/bin/env bash
# install and configure seevee dotfiles

repo_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
source "$repo_dir"/lib/bash-utils/index.sh

h1 "seevee dotfiles"
h1a "proceed with installation?"
read -r -n 1 -p "[y:N]: " confirm
printf "\n"
if ! [ "$confirm" = "y" ] || [ "$confirm" = "Y" ]; then
  err "exiting..."
  exit
fi
success "proceeding..."

mkdir -p "$repo_dir"/tmp
touch "$HOME"/.bash_profile

h1_emphasis "found OS: " "$OSTYPE"
h1_emphasis "found distro: " "$(get_linux_distribution)"

h1 "executable scripts"
sources=("scripts" "git")
mkdir -p "$HOME/bin"
for source in "${sources[@]}"; do
  h1a "in $repo_dir/$source/"
  for file in "$repo_dir"/"$source"/*; do
    filename_full=$(basename -- "$file")
    extension="${filename_full##*.}"
    filename="${filename_full%.*}"
    if [[ ${extension} != "sh" ]]; then
      info "skipping $file"
      continue
    fi
    h1b "linking $filename_full to ~/bin/$filename"
    overwrite_symlink "$file" "$HOME/bin/$filename"
  done
done
success "done"

h1 "dependencies"
h1a "common name dependencies"
h1b "installing via package manager"
install_packages cmake git curl unzip htop ack zsh fzf ripgrep \
  fontconfig binutils coreutils fastfetch tmux shellcheck lsd
success "done"

h1a "uncommon name dependencies"
h1b "installing via package manager"
case $(get_linux_distribution) in
  *ubuntu* | *pop*)       install_packages tk-dev ;;
  *arch* | *endeavouros*) install_packages tk ;;
  *)                      install_packages python-tk ;;
esac

h1 "git"
h1a "we need submodules (sorry)"
h1b "update recursively for this repo"
git submodule update --init --recursive
success "done"

h1a "global configuration"
h1b "including git/.gitconfig in ~/.gitconfig"
# gitconfig isn't smart enough to handle symlinks
case "$OSTYPE" in
  darwin*)  rlink_bin="greadlink" ;; # from coreutils
  *)        rlink_bin="readlink" ;;
esac
git config --global include.path "$($rlink_bin -e "$repo_dir"/git/.gitconfig)"
success "done"

h1 "zsh"
h1a "in $repo_dir/zsh/"
sources=(".zshrc" ".p10k.zsh")
for source in "${sources[@]}"; do
  h1b "linking $source to ~/$source"
  overwrite_symlink "$repo_dir/zsh/$source" "$HOME/$source"
done
success "done"

export ZSH="$repo_dir/zsh/oh-my-zsh"
h1a "in $ZSH"
h1b "installing via tools/install.sh"
sh -c "$ZSH/tools/install.sh"
success "done"

h1 "asdf"
h1a "in $repo_dir/asdf/"
sources=(".default-npm-packages" ".asdfrc")
for source in "${sources[@]}"; do
  h1b "linking $source to ~/$source"
  overwrite_symlink "$repo_dir/asdf/$source" "$HOME/$source"
done
success "done"

h1a "install / update"
asdf_dir=$HOME/.asdf
asdf_version=0.13.1
if hash asdf 2>/dev/null; then
  h1b "updating via asdf cli"
  asdf update
  success "done"

  h1b "adding java plugin"
  asdf plugin add java https://github.com/halcyon/asdf-java.git
  h1b "installing latest java"
  asdf install java latest:adoptopenjdk-20
  asdf global java latest:adoptopenjdk-20
  success "done"

  h1b "adding nodejs plugin"
  asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
  h1b "installing latest nodejs"
  asdf install nodejs latest
  asdf global nodejs latest
  success "done"

  h1b "adding rust plugin"
  asdf plugin add rust https://github.com/asdf-community/asdf-rust.git
  h1b "installing latest rust"
  RUST_WITHOUT=rust-docs asdf install rust latest
  asdf global rust latest
  success "done"
else
  h1b "installing to $asdf_dir via git"
  git clone https://github.com/asdf-vm/asdf.git \
    "$asdf_dir" --branch v$asdf_version
  success "done"
  info "restart your terminal and run the installer again"
  exit
fi

case "$OSTYPE" in
  darwin*)
    h1 "MacOS"
    h1a "additional dependencies"
    brew install font-hack-nerd-font font-sauce-code-pro-nerd-font
    success "done"
    ;;
  linux*)
    h1 "linux"

    h1 "xdg session type: $XDG_SESSION_TYPE"
    if [[ ${XDG_SESSION_TYPE} == "x11" ]]; then
      install_packages \
        i3-wm rofi feh scrot picom
      h1a "in $repo_dir/x11/"
      sources=(".Xresources" ".xinitrc")
      for source in "${sources[@]}"; do
        h1b "linking $source to ~/$source"
        overwrite_symlink "$repo_dir/x11/$source" "$HOME/$source"
      done
    else
      install_packages \
        sway rofi-wayland swaybg grim foot
    fi

    h1a "additional dependencies"
    install_packages \
      i3blocks dunst blueman pavucontrol-qt flameshot \
      imagemagick papirus-icon-theme
    success "done"

    h1 "application configuration"
    config_dir=$HOME/.config
    mkdir -p "$config_dir"

    h1a "in $repo_dir/"
    sources=("i3" "dunst" "rofi" "picom" "foot")
    for source in "${sources[@]}"; do
      h1b "linking $source to ~/.config/$source"
      overwrite_symlink "$repo_dir/$source" "$config_dir/$source"
    done
    h1b "linking i3 to ~/.config/sway"
    overwrite_symlink "$repo_dir/i3" "$config_dir/sway"
    success "done"

    case $(get_linux_distribution) in
      *arch* | *endeavouros*)
        h1b "installing betterlockscreen, nerd fonts from AUR via yay"
        yay -S \
          betterlockscreen ttf-hack-nerd ttf-sourcecodepro-nerd \
          --noconfirm --needed
        ;;
      *)
        h1b "installing Nerd fonts via git"
        declare -a fonts=(
          Hack
          SourceCodePro
        )
        tmp_dir="$repo_dir/tmp/nerd-fonts"
        git clone --filter=blob:none --sparse \
          https://github.com/ryanoasis/nerd-fonts.git "$tmp_dir"
        pushd "$tmp_dir" >/dev/null || exit
        for font in "${fonts[@]}"; do
          h1b "installing $font"
          git sparse-checkout add "patched-fonts/$font"
          ./install.sh "$font"
        done
        popd >/dev/null || exit
        success "done"

        i3lock_version="2.13.c.5"
        if [[ $(i3lock --version 2>&1 >/dev/null) == *"$i3lock_version"* ]]; then
          success "i3lock-color $i3lock_version already installed"
        else
          h1b "installing i3lock-color via git"
          tmp_dir="$repo_dir/tmp/i3lock-color-git"
          git clone https://github.com/Raymo111/i3lock-color.git "$tmp_dir"
          pushd "$tmp_dir" >/dev/null || exit
          git checkout "$i3lock_version"
          source ./install-i3lock-color.sh
          popd >/dev/null || exit
          success "done"
        fi

        h1b "installing betterlockscreen via wget"
        wget https://raw.githubusercontent.com/betterlockscreen/betterlockscreen/main/install.sh -O - -q | bash -s user
        success "done"
        ;;
    esac
    ;;
esac

# remove any tmp files
rm -rf "$repo_dir"/tmp
# source bashrc for good measure
source "$HOME"/.bashrc

h1 "submodules"
h1a "installation"
h1b "./vim"
# install vim submodule
source "$repo_dir"/vim/install.sh
success "submodule installation complete"

h1_emphasis "change your default terminal font to " "Hack Nerd Font" " now"
h1_success "dotfiles installation complete"
